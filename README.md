# Degordian zadatak - Vedran Murgić

*Zadatak*

> Napraviti mini borbu dvije vojske koja se sastoji samo od programerskog dijela u PHPu bez vidljivog sučelja. Igra je tekstualni "rat" dvije vojske. Vojske su sačinjene od N vojnika, N se dobiva iz $_GET-a sa ?army1=50&army2=48. Logiku borbe, strukturu vojske, tipove podataka, strukturu klasa, strukturu fileova, ispise ako ih ima, sve dodatne feature i opcije smišljaš sam. Jedna vlastita implementirana ideja je obavezna (može biti bilo što, generali, random potresi, vojnici polude, baš bilo što). Sve je dopušteno i u principu nema ograničenja koliko mali ili veliki cijeli program mora biti. Na kraju je bitno samo da se na neki način vidi koja vojska je pobijedila i zašto. :) 

*Finalno riješenje*

![live war](https://bytebucket.org/vmurgic/degordian-zadatak/raw/2935db7607cc0055816e1b6f22e2a9fce2804ecb/docs/images/war.gif)

# Kako radi #

Nakon primanja osnovnih get parametara generira se vojska sa brojem vojnika iz get parametra te se istoj toj vojsci na osnovu broja vojnika dodaje i:

- **generali** - 1 na svakih 50 vojnika
- **tenkovi** - random broj kojem je maksimum određen kao (broj_vojnika mod 10), minimum je 1
- **avioni** - kao i tenkovi ali je maksimum (broj_vojnika/10)
- **svinje** - vojnici moraju nešto i jesti. min je broj_vojnika, a max +50%.

Featuri rata dvije vojske

- Pobjeđuje vojska prva savlada sve protivničke generale.
- Generali stradavaju nakon što više nema vojnika, ali u nekim situacijama mogu stradati i prije nego im ponestane vojnika. npr: 
    - kada je neki powerup kreiran tako da može napasti generala direkno
	- kada 5 ili više vojnika u jednom napadu savlada svoje protivnike, oni dobivaju priliku da se bore s generalom
- svaki napad ima 5 jedinca koje se mogu podijeliti na:
    - hranjenje vojnika (1 svinja - 1 jedinica)
	- slanje vojnika u borbu (1 vojnik - 1 jedinica)
	- napad tenkom ili avionom (3 jedinice)
	- uzimanje ili korištenje powerupa (3 jedinice)
- powerup - pojavljuje se na bojištu i može pokupiti ona vojska koja je trenutno na redu.
- Kada napadaju tenk ili avio oni napadaju protivnički ekvivalent. Ukoliko ne postoji, tradavaju vojnici.
- Nakon svakog napada vojnici gube dio snage. Ovo je dio koji bi trebalo refaktorirati jer sam u zadnji tren vidio da na velikom broju vojnika se vrlo brzo pokaže 0.0%, a zapravo je bilo 0.000000005% pa prikaz nije bio baš fora. Uglavnom sad je bolje, ali trebalo bi to jasnije definirati.

Powerupovi:

- Male klase za sebe koje implementiraju Powerup sučelje.
- Nemaju ograničenja i mogu biti kreirane tako da djeluju jednom ili više puta. Nema nekih ograničenja jer znaju tko je napadač, a tko napadnuti te mogu vršiti akcije na obje vojske.

Trenutno implementirani powerupovi:

- **Rambo** - Provlači se kroz neprijateljsku liniju i u svakom napadu onesposobljuje jednog protivničkog vojnika. Nitko i ništa ne može ubiti Ramba. Rambo je sam za sebe pa ga ne treba ni hraniti. Jede on zmije i što već stigne putem. Ukratko, Rambo je frajer!
- **FemmeFatale** - Nakon 5 turnova javlja da je stupila u kontakt sa generalom i da će ga u ovom pokušati otrovati. General gubi sve živote koje je imao.
- **PigPoison** - Otrov za svinje instantno ubije 50% protivničkih svinja!

# It's war time
Pokreni index.php u browseru i dobit ćeš help koji će ti pomoći kako da pokreneš borbu.

![help](https://bytebucket.org/vmurgic/degordian-zadatak/raw/2935db7607cc0055816e1b6f22e2a9fce2804ecb/docs/images/help.PNG)

neagregirani rezultat

![no_agregate](https://bytebucket.org/vmurgic/degordian-zadatak/raw/2935db7607cc0055816e1b6f22e2a9fce2804ecb/docs/images/no_agregate.PNG)

agregirani rezultat

![agregate](https://bytebucket.org/vmurgic/degordian-zadatak/raw/2935db7607cc0055816e1b6f22e2a9fce2804ecb/docs/images/agregate.PNG)