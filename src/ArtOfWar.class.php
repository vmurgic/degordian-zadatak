<?php

require_once 'PowerUps.class.php';
require_once 'Army.class.php';

/**
 * Logic for the war is in this class.
 */

class ArtOfWar {
	const MAX_ACTIONS = 5;

	public $attacker;
	public $defender;
	private $powerups;
	private $soldiers_down_in_one_attack;
	private $attack_plan;
	public $round = 0;

	public function __construct ($army1, $army2) {
		$this->whoStrikesFirst($army1, $army2);
		// LOAD PLUGINS
		$this->powerups = new PowerUps();
	}

	private function resetAttackPlan() {
		$this->attack_plan = array(
			"food"=>0,
			"soldiers"=>0,
			"generals"=>0,
			"tanks"=>0,
			"planes"=>0,
			"pickup_powerup"=>false,
			"use_powerup"=>false
		);
	}

	public function attack() {
		$this->round++;
		WarStream::showRound($this->round);
		if (  $this->powerups->surprise_powerup === null ) $this->powerups->giveUsAPowerup();
		$this->soldiers_down_in_one_attack = 0;
		$this->resetAttackPlan();
		$this->planYourNextMove();
		WarStream::printAttackPlan($this->attacker, $this->attack_plan);
		$this->fire();
		$this->attacker->checkPowerups($this->defender);
		$this->switchAttacker();
	}

	public function whoStrikesFirst( $army1, $army2 ) {
		if ( $army1->no_of_soldiers === $army2->no_of_soldiers ) {
			// lets flip a coin
			$this->attacker = ( random_int(0,1) === 1 )?$army2:$army1;
			WarStream::whoStrikesFirst($this->attacker, true);
		} else {
			// army with less soldiers strikes first
			$this->attacker = ( $army1->no_of_soldiers > $army2->no_of_soldiers )?$army2:$army1;
			WarStream::whoStrikesFirst($this->attacker, false);
		}

		$this->defender = ($army1 === $this->attacker)?$army2:$army1;
	}

	public function switchAttacker() {
		$tmp = $this->attacker;
		$this->attacker = $this->defender;
		$this->defender = $tmp;
	}

	public function planYourNextMove(){
		$available_actions = $this::MAX_ACTIONS;

		// WILL YOU FEED THE SOLDIERS
		if ( $this->attacker->no_of_soldiers > 0 || count($this->attacker->generals) > 0 ) $this->attack_plan['food'] = $this->attacker->areYouHungry( $available_actions );
		// When there is no more food we will receive negative value which indicates that a soldier must die.
		if ( $this->attack_plan['food'] < 0 ) {
			if ( $this->attacker->no_of_soldiers > 0 ) {
				$this->attacker->killSoldier( 1 );
			} elseif ( count($this->attacker->generals) > 0 ) {
				$this->attacker->damageGeneral( 1 );
			}
			if ( count($this->attacker->generals) === 0 ) {
				WarStream::customMessage("<p>General (<span class='".$this->attacker->name."'>".$this->attacker->hr_name."</span>) je umro od gladi.</p>");
				return;
			}
			$this->attack_plan['food'] = 0;
		}
		$available_actions -= $this->attack_plan['food']; 
		if ( $available_actions === 0 ) return;

		// PICK UP POWERUP IF AVAILABLE
		if ( $available_actions >= 3 && $this->powerups->surprise_powerup != null && random_int(1, 100) > 50 ) {
			$this->attack_plan['pickup_powerup'] = true;
			$available_actions -= 3; 
			if ( $available_actions === 0 ) return;
		}

		// USE POWERUP OR NOT
		if ( $available_actions >= 5 && isset($this->attacker->powerup_ready) && $this->attacker->powerup_ready != null && random_int(1, 100) > 50 ) {
			$this->attack_plan['use_powerup'] = true;
			$available_actions -= 5; 
			if ( $available_actions === 0 ) return;
		}

		// USE MACHINERY
		if ( $available_actions >= 3 && ( $this->attacker->no_of_planes + $this->attacker->no_of_tanks ) > 0 ) {
			$max_possible_machines_to_use = floor($available_actions / 3);
			for ($i=0; $i < $max_possible_machines_to_use; $i++) { 
				// will you use this iteration to add machinery
				if ( random_int(0, 1) === 1 ) {
					// Only if coin says to use a tank and you really have a tank available - use it
					$use_tank = random_int(0, 1);
					if ( $use_tank === 1 && $this->attack_plan['tanks'] < $this->attacker->no_of_tanks ) {
						$this->attack_plan['tanks'] += 1;
						$available_actions -= 3;
					} elseif ( $use_tank === 0 && $this->attack_plan['planes'] < $this->attacker->no_of_planes ) {
						$this->attack_plan['planes'] += 1;
						$available_actions -= 3;
					}
				}
			} 
			if ( $available_actions === 0 ) return;
		}

		// NUMBER OF SOLDIERS ( or generals ) TO ATTACK - generals fight only when there are no soldiers left
		if ( $this->attacker->no_of_soldiers > 0 ) {
			$this->attack_plan['soldiers'] = ( $this->attacker->no_of_soldiers > $available_actions )?$available_actions:$this->attacker->no_of_soldiers;
			$available_actions -= $this->attack_plan['soldiers'];
		} else {
			$this->attack_plan['generals'] = ( count($this->attacker->generals) > $available_actions )?$available_actions:count($this->attacker->generals);
			$available_actions -= $this->attack_plan['generals'];
		}

		// If there is still more available_action add some machinery to the attack
		while ( $available_actions >= 3 && ( $this->attacker->hasPlanes() || $this->attacker->hasTanks() ) ) {
			if ( $this->attacker->hasPlanes() ) $this->attack_plan['planes'] += 1;
			if ( $this->attacker->hasTanks() ) $this->attack_plan['tanks'] += 1;
			$available_actions -= 3; 
		}
	}

	/**
	 * Method used to execute all planned actions
	 */
	private function fire () {        
		if ( $this->attack_plan['food'] > 0 ) {
			$this->attacker->feedTheArmy($this->attack_plan['food']);
		}

		if ( $this->attack_plan['pickup_powerup'] ) {
			$powerup_class = $this->powerups->grabAPowerup();
			$this->attacker->powerup_ready = new $powerup_class;
		}

		if ( $this->attack_plan['use_powerup'] ) {
			$this->attacker->usePowerup( $this->defender );
		}

		if ( $this->attack_plan['tanks'] > 0 ) {
			$this->attackMachine( Army::TANK, $this->attack_plan['tanks'], $this->attacker, $this->defender);
		}

		if ( $this->attack_plan['planes'] > 0 ) {
			$this->attackMachine( Army::PLANE, $this->attack_plan['planes'], $this->attacker, $this->defender);
		}

		if ( $this->attack_plan['soldiers'] > 0 ) {
			$this->attackSoldier( Army::SOLDIER, $this->attack_plan['soldiers'], $this->attacker, $this->defender);
		}

		if ( $this->attack_plan['generals'] > 0 ) {
			$this->attackSoldier( Army::GENERAL, $this->attack_plan['generals'], $this->attacker, $this->defender);
		}

		$this->attacker->loseEnergy();
	}

	public function attackSoldier( $attacker_type, $no_of_attackers, $attacker, $defender ) {
		$damage_level = 1;

		switch ($attacker_type) {
			case Army::PLANE:
				$damage_level = 2;
				break;

			case Army::TANK:
				$damage_level = 2;
				break;

			case Army::GENERAL:
				$damage_level = 2;
				break;
		}

		for ($i=0; $i < $no_of_attackers; $i++) { 
			if ( $defender->no_of_soldiers <= 0 ) {
				$this->attackGeneral( $attacker_type, $attacker, $defender );
				continue;
			}
			// simulate battle
			$battle_outcome = random_int(1, 100);
			if ( $battle_outcome < 51 ) {
				// attacker wins
				$defender->killSoldier($damage_level);
				$this->soldiers_down_in_one_attack++;
			} elseif ( $battle_outcome > 75 ) {
				// defender wins
				if ( $attacker_type === ARMY::SOLDIER ) {
					$attacker->killSoldier($damage_level);
				} elseif ( $attacker_type === ARMY::GENERAL ) {
					$is_general_down = $attacker->damageGeneral($damage_level);
				}
			}
		}
		
		// when 5 or more kills in one attack send wining soldiers to fight with the general
		if ( $attacker_type === Army::SOLDIER && $this->soldiers_down_in_one_attack >= 5 ) {
			echo "<p>5 ili više vojnika je savladalo svoje protivnike i sad imaju priliku napasti generala.</p>";
			for ($i=0; $i < $this->soldiers_down_in_one_attack; $i++) {
				$this->attackGeneral( $attacker_type, $attacker, $defender );
			}
		}
	}

	private function attackGeneral( $attacker_type, $attacker, $defender) {
		$damage_level = 1;

		switch ($attacker_type) {
			case Army::PLANE:
				$damage_level = 2;
				break;

			case Army::TANK:
				$damage_level = 2;
				break;

			case Army::GENERAL:
				$damage_level = 2;
				break;
		}

		if ( count($defender->generals) > 0 ) {
			$soldier_battle_outcome = random_int(1, 100);
			if ( $soldier_battle_outcome < 51 ) {
				$defender->damageGeneral($damage_level);
			} elseif ( $soldier_battle_outcome > 75 ) {
				// defender wins
				switch ($attacker_type) {
					case Army::SOLDIER:
						$attacker->killSoldier(1);
						break;
					case Army::GENERAL:
						$attacker->damageGeneral($damage_level);
						break;
					case Army::PLANE:
						$attacker->planeDown();
						break;
					case Army::TANK:
						$attacker->tankDown();
						break;
				}
			}
		}
	}

	private function attackMachine( $attacker_type, $no_of_attackers, $attacker, $defender ) {
		for ($i=0; $i < $no_of_attackers; $i++) { 
			if ( ($attacker_type === Army::TANK && !$defender->hasTanks()) || ($attacker_type === Army::PLANE && !$defender->hasPlanes()) ) {
				// Defender is attacked with the type of vehicle he doesn't have in his army and can't fight back. This results in his soldiers fighting against the vehicle.
				// When a vehicle attacks at least one soldiers gets eliminated. 
				$damage_level = random_int(1, 3); 
				$defender->killSoldier($damage_level);
			} else {
				$battle_outcome = random_int(1, 100);
				if ( $battle_outcome < 51 ) {
					if ( $attacker_type === Army::TANK && $defender->no_of_tanks > 0 ) {
						$defender->tankDown();
					} elseif ( $attacker_type === Army::PLANE && $defender->no_of_planes > 0 ) {
						$defender->planeDown();
					}
				} elseif ( $battle_outcome > 75 ) {
					if ( $attacker_type === Army::TANK ) {
						$defender->tankDown();
					} elseif ( $attacker_type === Army::PLANE ) {
						$defender->planeDown();
					}
				}
			}
		}
	}
}