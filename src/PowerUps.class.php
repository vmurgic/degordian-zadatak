<?php

require_once 'WarStream.class.php';

class PowerUps {
	// List of all loaded powerups from powerups path.
	public $available_powerups = array();
	// Random powerup that will be available to the army to pick up and use.
	public $surprise_powerup = null;

	/**
	 * Constructor is used to load all the available powerups.
	 */
	public function __construct () {
		$powerups_dir = __DIR__."/powerups";
		$files = glob($powerups_dir . '/*.powerup.php');
		foreach ($files as $file) {
			require($file);
			preg_match("/(\w+).class.powerup.php/", $file, $match);
			if ( $match[1]::USE_ME ) {
				$this->available_powerups[] = $match[1];
			}
		}
		$this->showAvailablePowerUps();
	}

	public function showAvailablePowerUps () {
		if ( count($this->available_powerups) > 0 ){
			echo "<p>Slijedeći powerupovi se mogu pojaviti na bojištu:</p>";
			foreach ( $this->available_powerups as $powerup ) {
				echo "<p class='indent'><i>$powerup</i> - ".$powerup::getDescription()."</p>";
			}
		}
	}

	/**
	 * Method will throw random powerup into battle field.
	 * There is a 50% chance of a powerup being thrown.
	 */
	public function giveUsAPowerup () {
		if ( count($this->available_powerups) && random_int(1, 100) > 50 ) {
			$this->surprise_powerup = $this->available_powerups[random_int(0, count($this->available_powerups)-1 )];
			WarStream::customMessage("<p>Powerup iznenađenja je na bojištu! Zgrabi ga prije protivnika!</p>");
		}
	}

	public function grabAPowerup () {
		$powerup = $this->surprise_powerup;
		$this->surprise_powerup = null;
		WarStream::customMessage("<p>Dobar ulov! Pokupio si powerup - ".$powerup::getName()."</p>");
		return $powerup;
	}
}