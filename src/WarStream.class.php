<?php

/**
 * Static class used to populate the war log with data.
 */

class WarStream {
	const STATS = true;
	private static $is_initial_status_printed = false;
	private static $agregate = true;

	/**
	 * Method prints help to the user where he is provided with all the obligatory and optional parameters that can be passed to the war.
	 * @param bool $zero_soldiers Prints different help in case zero soldiers are provided.
	 */
	public static function help ( $zero_soldiers ) {
		echo "<div id='help'>";
		if ( $zero_soldiers ){
			echo "<p>Kako ćeš pobjediti kad nemaš vojnika!?</p>";
			echo "<p>Nađi neke plaćenike barem i pošalji ih u borbu!</p>";
		} else {
			echo "<p>--- Čuvaj položaj! Pomoć je na putu! ---</p><hr/>";
			echo "<p>Dva su obavezna parametra koja mi moraš proslijediti:</p>";
			echo "<p class='indent'><i>army1</i> - prima cijeli broj i postavlja broj vojnika u prvoj vojsci</p>";
			echo "<p class='indent'><i>army2</i> - prima cijeli broj i postavlja broj vojnika u drugojs vojsci</p>";
			echo "<p>npr. <i>?army1=34&army2=56</i></p><hr/>";
			echo "<p>Osim ova dva obavezna, primam i slijedeće parametre:</p>";
			echo "<p class='indent'><i>slomo</i> - Prima cijeli broj i na osnovu njega odgađa slijedeći napad na toliko sekundi kako bi se lakše pratio tijek rata. Default: 0.</p>";
			echo "<p class='indent'><i>agregate</i> - Prima true/false. Default: true. Kada je true, prikazuje jedan semafor sa rezultatima i osvježava ga. U protivnom u live logu ispisuje novi semafor nakon svakog koraka.</p>";
			echo "<p>npr. <i>?army1=34&army2=56&slomo=1</i></p><hr/>"; 
			echo "<p>primjeri</p>";
			echo "<p><a href='index.php?army1=50&army2=50'>?army1=50&army2=50</a></p>";
			echo "<p><a href='index.php?army1=45&army2=52&slomo=1'>?army1=45&army2=52&slomo=1</a></p>";
			echo "<p><a href='index.php?army1=99&army2=85&slomo=1&agregate=false'>?army1=99&army2=85&slomo=1&agregate=false</a></p>";
		}
		echo "</div>";
	}

	public static function setAgregate ( $agregate ) {
		self::$agregate = $agregate;
	}

	public static function startTheWar () {
		echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--- Let the war begin! ---<br/>";
	}

	private static function printInitialStatus($army1, $army2){
		echo "<div class='start'>";
		echo sprintf("<span class='red'>Crvena vojska --> </span>#v: %d, #g: %d, #a: %d, #t: %d, #s: %d<br/><span class='blue'>Plava vojska --> </span>#v: %d, #g: %d, #a: %d, #t: %d, #s: %d", $army1->no_of_soldiers, sizeof($army1->generals), $army1->no_of_planes, $army1->no_of_tanks, $army1->no_of_pigs, $army2->no_of_soldiers, sizeof($army2->generals), $army2->no_of_planes, $army2->no_of_tanks, $army2->no_of_pigs);
		echo "</div>";
		self::$is_initial_status_printed = true;
	}

	public static function printArmyStats($army1, $army2) {
		if (!self::$is_initial_status_printed) self::printInitialStatus($army1, $army2);

		$stats_class = ( self::$agregate )?"stats":"";

		echo "<div class='$stats_class'>";
		echo sprintf("%'-40s <br/>", "");
		echo str_replace(' ', '&nbsp;', sprintf("| %10s | %10s | %10s | <br/>", "", "Crveni", "Plavi"));
		echo sprintf("|%'-38s| <br/>","");
		echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "#vojnika", $army1->no_of_soldiers."(".number_format($army1->energy*100, 1)."%)", $army2->no_of_soldiers."(".number_format($army2->energy*100, 1)."%)"));
		echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "#generala", sizeof($army1->generals), sizeof($army2->generals)));
		echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "#aviona", $army1->no_of_planes, $army2->no_of_planes));
		echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "#tenkova", $army1->no_of_tanks, $army2->no_of_tanks));
		echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "#svinja", $army1->no_of_pigs, $army2->no_of_pigs));
		// echo str_replace(' ', '&nbsp;', sprintf("| %-10s | %10s | %10s | <br/>", "powerups", $army1->powerups, $army2->powerups));
		echo sprintf("%'-40s <br />","");
		echo "<p>Pobjednik: ".self::whoWon($army1, $army2)."<p>";
		echo "<br/><p>War log<p><hr/>";
		echo "</div>";
	}

	public static function whoStrikesFirst($attacker, $decided_by_coin) {
		if ( $decided_by_coin ) {
			echo sprintf("Obzirom da obje vojske imaju jednaki broj vojnika novčić je odlučio da će prva napasti <span class='%s'>%s</span>!<br/>", $attacker->name, $attacker->hr_name );
		} else {
			echo sprintf("<span class='%s'>%s</span> napada prva jer ima manje vojnika</span>!<br/>", $attacker->name, $attacker->hr_name );
		}
	}

	public static function whoWon($army1, $army2){
		$won;
		if ( sizeof($army1->generals) <= 0 ) {
			$won = $army2;
		} elseif ( sizeof($army2->generals) <= 0 ) {
			$won = $army1;
		}
		if( isset($won) ) return "<span class='".$won->name."'>".$won->hr_name."</span>";
		else return "Bitka još traje...";
	}

	public static function printAttackPlan( $attacker, $attack_plan ) {
		echo sprintf("<p>%s je u napadu:<p>", $attacker->hr_name);

		if ( $attack_plan['use_powerup'] ) echo "<p class='indent'>koristim powerup - ".get_class($attacker->powerup_ready).".</p>";
		if ( $attack_plan['pickup_powerup'] ) echo "<p class='indent'>pokupit ću powerup.</p>";
		if ( $attack_plan['food'] > 0 ) echo "<p class='indent'>#svinja za ručak: ".$attack_plan['food']."</p>";
		if ( $attack_plan['soldiers'] > 0 ) echo "<p class='indent'>#vojnika u borbi: ".$attack_plan['soldiers']."</p>";
		if ( $attack_plan['generals'] > 0 ) echo "<p class='indent'>#generala u borbi: ".$attack_plan['generals']."</p>";
		if ( $attack_plan['tanks'] > 0 ) echo "<p class='indent'>#tenkova u borbi: ".$attack_plan['tanks']."</p>";
		if ( $attack_plan['planes'] > 0 ) echo "<p class='indent'>#aviona u borbi: ".$attack_plan['planes']."</p>";
	}

	public static function showRound ( $round ) {
		echo "<p class='green'>.............::: Round $round :::.............</p>";
	}

	/**
	 * Enables any message to be printed to the war log.
	 * @param string $message
	 */
	public static function customMessage ( $message ) {
		echo $message;
	}
}