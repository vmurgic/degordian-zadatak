<?php

/**
 * Degordian zadatak
 * Napraviti mini borbu dvije vojske koja se sastoji samo od programerskog dijela u PHPu bez vidljivog sučelja. 
 * Igra je tekstualni "rat" dvije vojske. Vojske su sačinjene od N vojnika, N se dobiva iz $_GET-a sa ?army1=50&army2=48. 
 * Logiku borbe, strukturu vojske, tipove podataka, strukturu klasa, strukturu fileova, ispise ako ih ima, sve dodatne feature i opcije smišljaš sam. 
 * Jedna vlastita implementirana ideja je obavezna (može biti bilo što, generali, random potresi, vojnici polude, baš bilo što). 
 * Sve je dopušteno i u principu nema ograničenja koliko mali ili veliki cijeli program mora biti. 
 * Na kraju je bitno samo da se na neki način vidi koja vojska je pobijedila i zašto. :) 
 * @author Vedran Murgić <vedran.murgic@gmail.com>
 */

@ini_set('zlib.output_compression',0);
@ini_set('implicit_flush',1);
@ob_end_clean();
set_time_limit(0);
ob_implicit_flush(1);

// Defined in seconds. Sets a delay between attacks so humans can watch the battle turn by turn.
$slomo = ( empty( $_GET['slomo'] ) )?0:$_GET['slomo'];
// If enabled then scoreboard will be on top and will have a live update. Otherwise it will be printed out in the war log.
$agregate = ( empty( $_GET['agregate'] ) )?true:filter_var($_GET['agregate'], FILTER_VALIDATE_BOOLEAN);
?>

<!doctype html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="stylesheet" href="style/console.css" />
		<title>Vedran Murgić ::: Degordian zadatak</title>
	</head>
	<body class="<?php echo ($agregate)?'padding-top':''; ?>">

<?php 

require_once 'Army.class.php';
require_once 'WarStream.class.php';
require_once 'ArtOfWar.class.php';

WarStream::setAgregate($agregate);

if ( empty( $_GET['army1'] ) || empty( $_GET['army2'] ) ) {
	$zero_soldiers = false;
	if ( ( isset($_GET['army1']) && $_GET['army1'] == 0 ) || ( isset($_GET['army2']) && $_GET['army2'] == 0 ) ) {
		$zero_soldiers = true;
	}
	WarStream::help( $zero_soldiers );
} else {
	$army1 = new Army($_GET['army1'], "red", "Crvena vojska");
	$army2 = new Army($_GET['army2'], "blue", "Plava vojska");

	WarStream::printArmyStats($army1, $army2);

	$art_of_war = new ArtOfWar($army1, $army2);

	WarStream::startTheWar();

	// War ends when any army loses all its generals. Some wars never end and this loop can therefore run forever. There is no safe net in the war which is why this loop hill not have any safe net.
	while ( sizeof($army1->generals) > 0 && sizeof($army2->generals) > 0 ) {
		$art_of_war->attack();
		sleep($slomo); // just so we can easily track whats going on in the war
		WarStream::printArmyStats($army1, $army2);
	}
}
?>

	</body>
</html>