<?php

require_once 'WarStream.class.php';
require_once 'PowerUp.interface.php';
require_once 'AbstractPowerUp.class.php';

class FemmeFatale extends AbstractPowerUp implements PowerUp {
	CONST USE_ME = true;
	public $used = false;
	private $target;
	private $counter = 0;

	protected static $description = "Nakon 5 turnova javlja da je stupila u kontakt sa generalom i da će ga u ovom pokušati otrovati. General gubi sve živote koje je imao.";

	public function fireUp ( $target ) {
		if ( !$this->used ) {
			$this->target = $target;
			$this->used = true;
		} else {
			WarStream::customMessage("<p>Femme Fatale je već na terenu!</p>");
		}
	}

	/**
	 * 5 turns after the plugin is used Femme Fatale will try to poison the general. Chance of success is 40%.
	 */
	public function checkStatus () {
		$this->counter++;
		if ( $this->counter === 5 && count($this->target->generals) > 0 ) {
			WarStream::customMessage("<p>".self::getName().": Stupila sam u vezu sa generalom! Pokušat ću ga otrovati!</p>");
			if ( random_int(1, 10) > 6 ) {
				array_splice($this->target->generals, 0, 1);
				WarStream::customMessage("<p>".self::getName().": Uspjela sam otrovati generala!</p>");
			} else {
				WarStream::customMessage("<p>".self::getName().": Trovanje generala nije uspjelo!</p>");
			}
		}
	}
}