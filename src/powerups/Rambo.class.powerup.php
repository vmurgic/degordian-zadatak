<?php

require_once 'WarStream.class.php';
require_once 'PowerUp.interface.php';
require_once 'AbstractPowerUp.class.php';

class Rambo extends AbstractPowerUp implements PowerUp {
	CONST USE_ME = true;
	public $used = false;
	private $target;
	private $counter = 0;

	protected static $description = "Provlači se kroz neprijateljsku liniju i u svakom napadu onesposobljuje jednog protivničkog vojnika. Nitko i ništa ne može ubiti Ramba. Rambo je sam za sebe pa ga ne treba ni hraniti. Jede on zmije i što već stigne putem. Ukratko, Rambo je frajer!";

	public function fireUp ( $target ) {
		if ( !$this->used ) {
			$this->target = $target;
			$this->used = true;
		} else {
			WarStream::customMessage("<p>Sorry! Ovaj powerup si već ispucao!</p>");
		}
	}

	public function checkStatus () {
		WarStream::customMessage("<p>Rambo je onesposobio još jednoga</p>");
		if ( $this->target->no_of_soldiers > 0 ) $this->target->killSoldier(1);
		else $this->target->damageGeneral(1);
		$this->counter++;
	}
}