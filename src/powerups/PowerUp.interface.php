<?php

interface PowerUp {
	/**
	 * Used when powerup is first used. You can set anything you want inside this method.
	 * @param Army $target Defines an army you want to target. 
	 */
	public function fireUp( $target );

	/**
	 * Method that will be run in each attack on every plugin ever used by an army. Here you can define any additional damage a plugin is designed to do after it is fired up.
	 */
	public function checkStatus();
}