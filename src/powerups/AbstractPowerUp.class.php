<?php

abstract class AbstractPowerUp {

	public static function getDescription () {
		return static::$description;
	}

	public static function getName () {
		return get_called_class();
	}
}