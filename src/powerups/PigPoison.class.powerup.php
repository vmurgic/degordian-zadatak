<?php

require_once 'WarStream.class.php';
require_once 'PowerUp.interface.php';
require_once 'AbstractPowerUp.class.php';

class PigPoison extends AbstractPowerUp implements PowerUp {
	CONST USE_ME = true;
	public $used = false;

	protected static $description = "Otrov za svinje instantno ubije 50% protivničkih svinja!";

	public function fireUp ( $target ) {
		if ( !$this->used ) {
			$target->no_of_pigs = ceil($target->no_of_pigs / 2);
			$this->used = true;
		} else {
			WarStream::customMessage("<p>Sorry! Ovaj powerup si već ispucao!</p>");
		}
	}

	public function checkStatus () {
		// This powerup hits instantly and then it's done.
	}
}