<?php

require_once 'WarStream.class.php';

class Army {
	const ENERGY_LOST_AFTER_EVERY_ATTACK = 1;
	const SOLDIER = 0;
	const GENERAL = 1;
	const PLANE = 2;
	const TANK = 3;

	public $no_of_soldiers;
	public $generals = array();
	public $no_of_planes;
	public $no_of_tanks;
	public $no_of_pigs;
	public $energy = 1;
	// you can have only one powerup waiting to be used
	public $powerup_ready;
	public $powerups_used = array();
	public $name;
	public $hr_name;
	private $soldiers_are_starving = false;
	
	/**
	 * Perfect place to generate an army.
	 * @param int $no_of_soldiers
	 * @param string $name Give your army a name. This name will be used for css class name.
	 * @param string $hr_name Give it also a croatian name so it can be printed out.
	 */
	public function __construct( $no_of_soldiers, $name, $hr_name ) {
		$this->no_of_soldiers = $no_of_soldiers;
		$this->name = $name;
		$this->hr_name = $hr_name;
		
		// There is one general on every 50 soldiers
		$no_of_generals = ceil( $no_of_soldiers / 50 );
		for ($i=0; $i < $no_of_generals; $i++) { 
			// Every general has 5 lives
			$this->generals[] = 5;
		}

		// #ofPlanes is a random number with max value determined by #ofSoldiers / 10
		$max_planes = ( ($no_of_soldiers / 10) > 0 )?$no_of_soldiers / 10:1;
		$this->no_of_planes = random_int(1, $max_planes);

		// #ofTanks is a random number with max value determined by #ofSoldiers % 10
		$max_tanks = ( ($no_of_soldiers % 10) > 0 )?$no_of_soldiers % 10:1;
		$this->no_of_tanks = random_int(1, $max_tanks);
		
		// #ofPigs is a random number with max value +50% no_of_soldiers
		$this->no_of_pigs = random_int($no_of_soldiers, $no_of_soldiers*1.5);
	}

	public function killSoldier( $count = 1 ) {
		$this->no_of_soldiers -= $count;
		// In case there is a strike from the vehicle it can damage soldiers more than one and if we have only 1 soldier this would mean we would have negative number of soldiers which is kind of odd.
		if ( $this->no_of_soldiers < 0 ) $this->no_of_soldiers = 0;
	}

	public function loseEnergy() {
		$lose_in_this_turn = $this::ENERGY_LOST_AFTER_EVERY_ATTACK / ($this->no_of_soldiers / 2 + 1);
		if( $lose_in_this_turn > 0.1 ) $lose_in_this_turn = 0.1;
		$this->energy = $this->energy - $lose_in_this_turn;
		if ( $this->energy < 0 ) $this->energy = 0;
	}

	public function damageGeneral ( $damage_level ) {
		$general_down = false;
		if ( count($this->generals) === 0 ) return false;
		$this->generals[0] = $this->generals[0] - $damage_level;

		// if general loses all his lives then remove him from the list
		if ( $this->generals[0] <= 0 ) {
			array_splice($this->generals, 0, 1);
			$general_down = true;
		}

		return $general_down;
	}

	public function planeDown () {
		$this->no_of_planes--;
	}

	public function tankDown () {
		$this->no_of_tanks--;
	}

	/**
	 * Feed the soldiers when energy level drops below 21%.
	 * Or ask them if they want to eat something in case energy level is below 81%.
	 * @param int $max_food The maximum amount of food that can be returned as a result.
	 */
	public function areYouHungry ( $max_food ) {
		$feed_with = 0;
		if ( $this->no_of_pigs == 0 ) {
			if ( !$this->soldiers_are_starving ) {
				WarStream::customMessage("<p>Nema više hrane... U svakom napadu od gladi će umrijeti 1 vojnik!</p>");
				$this->soldiers_are_starving = true;
			} else {
				return -1;
			}
			return 0;
		} elseif ( $this->energy <= 0.2 ) {
			$max_food_needed = ceil( ( $this->no_of_soldiers + count( $this->generals ) ) * ( 1 - $this->energy ) );
			if ( $this->no_of_pigs < $max_food_needed ) $max_food_needed = $this->no_of_pigs;
			if ( $max_food_needed < 1 )$max_food_needed = 1;
			$feed_with = random_int(1, $max_food_needed);
		} elseif ( $this->energy <= 0.8 && random_int(1, 100) > 50 ) {
			$max_food_needed = ceil( ( $this->no_of_soldiers + count( $this->generals ) ) * ( 1 - $this->energy ) );
			if ( $this->no_of_pigs < $max_food_needed ) $max_food_needed = $this->no_of_pigs;
			// Do not let them eat too much if they are not too hungry - give them half, they will fight easier! :D
			$max_food_needed = ceil($max_food_needed / 2);
			if ( $max_food_needed < 1 )$max_food_needed = 1;
			$feed_with = random_int(1, $max_food_needed);
		}
		if ( $feed_with > $max_food ) $feed_with = $max_food;
		return $feed_with;
	}

	public function feedTheArmy ( $pigs_for_lunch ) {
		$total_mouths_to_feed = $this->no_of_soldiers + count($this->generals);
		$this->energy += $pigs_for_lunch / $total_mouths_to_feed;
		if ( $this->energy > 1 ) $this->energy = 1;
		$this->no_of_pigs -= $pigs_for_lunch;
	}

	public function hasPlanes () {
		if ( $this->no_of_planes > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	public function hasTanks () {
		if ( $this->no_of_tanks > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used when army wants to use a powerup that is available in $powerup_ready var.
	 * @param Army $target Defines army you are targetting.
	 */
	public function usePowerup ( $target ) {
		$this->powerup_ready->fireUp( $target );
		$this->powerups_used[] = $this->powerup_ready;
		unset($this->powerup_ready);
	}

	/**
	 * Used to iterate over used powerups to signal them there is a new attack.
	 * Some powerups can have this method empty which means they do not caouse any damage other than initial one when they are used.
	 * @param Army $target Defines army you are targetting.
	 */
	public function checkPowerups ( $target ) {
		WarStream::customMessage("<p>Iskorištenih powerupova: ".count($this->powerups_used)."</p>");
		foreach ($this->powerups_used as $powerup) {
			$powerup->checkStatus();
		}
	}
}